# 4C | ProgramasDAPPS
## Diseño de Apps 
> Saldaña Espinoza Hector - Desarrollo de Software
>

## Contenido 🚀
#### IntelliJ 2020.1 🤖⚙️

* ProgramasDIAPPS - Ejercicios con código
  - [JAVA](https://github.com/HectorSaldes/ProgramasDAPPS/tree/master/ProgramasDIAPPS/src/JAVA) - Repaso de Java
  - [PLATZI](https://github.com/HectorSaldes/ProgramasDAPPS/tree/master/ProgramasDIAPPS/src/PLATZI) - Curso de Kotlin en Platzi
  - [UTEZ](https://github.com/HectorSaldes/ProgramasDAPPS/tree/master/ProgramasDIAPPS/src/UTEZ) - Aprendiendo Kotlin en la Escuela

#### Android Studio ✔⚙️
* [MiPrimeraApp](https://github.com/HectorSaldes/ProgramasDAPPS/tree/master/MiPrimeraApp) - Primera aplicación lanzada en celular
* [MiCalculadora](https://github.com/HectorSaldes/ProgramasDAPPS/tree/master/MiCalculadora) - Pequeña calculadora
* [CalculadorDivisas](https://github.com/HectorSaldes/ProgramasDAPPS/tree/master/CalculadorDivisas) - Pequeña calculadora
* [AgenciaDeCoches](https://github.com/HectorSaldes/ProgramasDAPPS/tree/master/AgenciaDeCoches) - Agencia de Coches Tesla

