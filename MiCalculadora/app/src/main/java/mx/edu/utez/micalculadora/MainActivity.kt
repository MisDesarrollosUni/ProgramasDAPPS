package mx.edu.utez.micalculadora

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    //ESTA CLASE ES UNA ESCUCHA DE BOTOTNES


    private var primerOperando:Double=0.0
    private var num2:Double=0.0
    private var operacion:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //A TODOS LOS BOTONES SE LES DICE QUE TENDRAN LO DE ONCLICK
        btn0.setOnClickListener(this)
        btn1.setOnClickListener(this)
        btn2.setOnClickListener(this)
        btn3.setOnClickListener(this)
        btn4.setOnClickListener(this)
        btn5.setOnClickListener(this)
        btn6.setOnClickListener(this)
        btn7.setOnClickListener(this)
        btn8.setOnClickListener(this)
        btn9.setOnClickListener(this)
        btnPunto.setOnClickListener(this)
        btnSumar.setOnClickListener(this)
        btnRestar.setOnClickListener(this)
        btnMultiplicacion.setOnClickListener(this)
        btnDivision.setOnClickListener(this)
        btnResultado.setOnClickListener(this)
        btnLimpiar.setOnClickListener(this)

    }

    //TODOS LOS BOTONES SE CONCENTRAN AQUI, DEPENDIENDO DE SU CASO
    override fun onClick(boton: View?) {
        when(boton?.id){
            R.id.btn0 ->{
                txtCampo.setText(if(txtCampo.text.toString().equals("0")) "0" else "${txtCampo.text}0")
            }
            R.id.btn1 ->{
                txtCampo.setText(if(txtCampo.text.toString().equals("0")) "1" else "${txtCampo.text}1")
            }
            R.id.btn2 ->{
                txtCampo.setText(if(txtCampo.text.toString().equals("0")) "2" else "${txtCampo.text}2")
            }
            R.id.btn3 ->{
                txtCampo.setText(if(txtCampo.text.toString().equals("0")) "3" else "${txtCampo.text}3")
            }
            R.id.btn4 ->{
                txtCampo.setText(if(txtCampo.text.toString().equals("0")) "4" else "${txtCampo.text}4")
            }
            R.id.btn5 ->{
                txtCampo.setText(if(txtCampo.text.toString().equals("0")) "5" else "${txtCampo.text}5")
            }
            R.id.btn6 ->{
                txtCampo.setText(if(txtCampo.text.toString().equals("0")) "6" else "${txtCampo.text}6")
            }
            R.id.btn7 ->{
                txtCampo.setText(if(txtCampo.text.toString().equals("0")) "7" else "${txtCampo.text}7")
            }
            R.id.btn8 ->{
                txtCampo.setText(if(txtCampo.text.toString().equals("0")) "8" else "${txtCampo.text}8")
            }
            R.id.btn9 ->{
                txtCampo.setText(if(txtCampo.text.toString().equals("0")) "9" else "${txtCampo.text}9")
            }
            R.id.btnPunto ->{
                if(txtCampo.text.toString().contains(".")){
                    Toast.makeText(this,"No puedes escribir más de un punto",Toast.LENGTH_LONG).show()
                }else{
                    txtCampo.setText("${txtCampo.text}.")
                }
            }
            R.id.btnSumar ->{
                primerOperando = txtCampo.text.toString().toDouble()
                txtCampo.setText("0")
                operacion = "+"

            }
            R.id.btnRestar ->{
                primerOperando = txtCampo.text.toString().toDouble()
                txtCampo.setText("0")
                operacion = "-"

            }

            R.id.btnMultiplicacion ->{
                primerOperando = txtCampo.text.toString().toDouble()
                txtCampo.setText("0")
                operacion = "*"

            }
            R.id.btnDivision ->{
                primerOperando = txtCampo.text.toString().toDouble()
                txtCampo.setText("0")
                operacion = "/"

            }

            R.id.btnResultado->{
               when(operacion){
                   "+" -> {
                       txtCampo.setText("${primerOperando + txtCampo.text.toString().toDouble()}")
                   }
                   "-" -> {
                       txtCampo.setText("${primerOperando - txtCampo.text.toString().toDouble()}")
                   }
                   "*" -> {
                       txtCampo.setText("${primerOperando * txtCampo.text.toString().toDouble()}")
                   }
                   "/" -> {
                      if(txtCampo.text.toString().equals("0") || txtCampo.text.toString().equals("0.0")){
                          Toast.makeText(this,"No pueden divir entre 0",Toast.LENGTH_LONG).show()
                      }else{
                          txtCampo.setText("${primerOperando / txtCampo.text.toString().toDouble()}")
                      }
                   }
               }
            }
            R.id.btnLimpiar ->{
                txtCampo.setText("0")
            }
        }
    }

}
