package mx.edu.utez.calculadordivisas

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import mx.edu.utez.calculadordivisas.R.array.divisas
import java.text.DecimalFormat

class Main : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var adaptacion:ArrayAdapter<String> = ArrayAdapter(this@Main,R.layout.spn_elemento_divisa, resources.getStringArray((R.array.divisas)))
        adaptacion.setDropDownViewResource(R.layout.spn_elementos_divisas)
        spnDivisa1.adapter = adaptacion
        spnDivisa2.adapter = adaptacion

        var divisa1 = -1
        var divisa2 = -1
        var resultado = 0.0
        var simbolo = ""

        val MXN = "MXN"
        val USD = "$"
        val EUR = "€"
        val JPY = "¥"
        val GBP = "£"

        val mxn_a_dolar = 0.049
        val mxn_a_euro = 0.041
        val mxn_a_yen = 5.02
        val mxn_a_libra = 0.037

        val dolar_a_mxn = 20.58
        val dolar_a_euro = 0.84
        val dolar_a_yen = 103.35
        val dolar_a_libra = 0.76

        val euro_a_mxn = 24.44
        val euro_a_dolar = 1.19
        val euro_a_yen = 122.74
        val euro_a_libra = 0.90

        val yen_a_mxn = 0.20
        val yen_a_dolar = 0.0097
        val yen_a_euro = 0.0081
        val yen_a_libra = 0.0074

        val libra_a_mxn = 27.08
        val libra_a_dolar = 1.32
        val libra_a_euro = 1.11
        val libra_a_yen = 135.98

        btnLimpiar.setOnClickListener{
            txtCantidad.text.clear();
            txtResultado.setText(resources.getString(R.string.valorInicial))
            spnDivisa1.setSelection(0)
            spnDivisa2.setSelection(0)
        }

        spnDivisa1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(this@Main, "Falta seleccionar divisa", Toast.LENGTH_LONG).show()
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                divisa1 = position
               // Toast.makeText(this@Main, "${spnDivisa1.selectedItem.toString()} en la Poscicion $divisa1", Toast.LENGTH_LONG).show()
            }
        }

        spnDivisa2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(this@Main, "Falta seleccionar divisa", Toast.LENGTH_LONG).show()
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                divisa2 = position
                //Toast.makeText(this@Main, "${spnDivisa2.selectedItem.toString()} en la Poscicion $divisa2", Toast.LENGTH_LONG).show()
            }
        }

        btnConvertir.setOnClickListener {
            if (divisa1 == 0 || divisa2 == 0 || txtCantidad.text.isEmpty()) {
                txtResultado.setText(resources.getString(R.string.valorInicial))
                Toast.makeText(this@Main, "Hace falta seleccionar", Toast.LENGTH_LONG).show()
            } else {
                var txtCantidadUI = txtCantidad.text.toString().toDouble()
                when (divisa1) {
                    1 -> {
                        when (divisa2) {
                            1 -> resultado = txtCantidadUI
                            2 -> resultado = txtCantidadUI * mxn_a_dolar
                            3 -> resultado = txtCantidadUI * mxn_a_euro
                            4 -> resultado = txtCantidadUI * mxn_a_yen
                            5 -> resultado = txtCantidadUI * mxn_a_libra
                        }
                    }
                    2 -> {
                        when (divisa2) {
                            1 -> resultado = txtCantidadUI * dolar_a_mxn
                            2 -> resultado = txtCantidadUI
                            3 -> resultado = txtCantidadUI * dolar_a_euro
                            4 -> resultado = txtCantidadUI * dolar_a_yen
                            5 -> resultado = txtCantidadUI * dolar_a_libra
                        }
                    }
                    3 -> {
                        when (divisa2) {
                            1 -> resultado = txtCantidadUI * euro_a_mxn
                            2 -> resultado = txtCantidadUI * euro_a_dolar
                            3 -> resultado = txtCantidadUI
                            4 -> resultado = txtCantidadUI * euro_a_yen
                            5 -> resultado = txtCantidadUI * euro_a_libra
                        }
                    }
                    4 -> {
                        when (divisa2) {
                            1 -> resultado = txtCantidadUI * yen_a_mxn
                            2 -> resultado = txtCantidadUI * yen_a_dolar
                            3 -> resultado = txtCantidadUI * yen_a_euro
                            4 -> resultado = txtCantidadUI
                            5 -> resultado = txtCantidadUI * yen_a_libra
                        }
                    }
                    5 -> {
                        when (divisa2) {
                            1 -> resultado = txtCantidadUI * libra_a_mxn
                            2 -> resultado = txtCantidadUI * libra_a_dolar
                            3 -> resultado = txtCantidadUI * libra_a_euro
                            4 -> resultado = txtCantidadUI * libra_a_yen
                            5 -> resultado = txtCantidadUI
                        }
                    }
                }

                when (divisa2) {
                    1 -> simbolo = MXN
                    2 -> simbolo = USD
                    3 -> simbolo = EUR
                    4 -> simbolo = JPY
                    5 -> simbolo = GBP
                }
            }
            txtResultado.setText("\$ ${String.format("%.2f",resultado)} $simbolo")
            resultado = 0.0
            simbolo =""
        }

    }


}
