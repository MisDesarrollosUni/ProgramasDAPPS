package mx.edu.utez.miprimeraapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var txtUsuarioUI = findViewById<EditText>(R.id.txtUsuario)
        var txtContrasenaUI = findViewById<EditText>(R.id.txtContrasena)

        var btnIniciarUI = findViewById<Button>(R.id.btnIniciar)
        var btnLimpiarUI = findViewById<Button>(R.id.btnLimpiar)



        btnIniciarUI.setOnClickListener {
            if(txtUsuarioUI.text.toString().equals("Hector") && txtContrasenaUI.text.toString().equals("123456")){
                Toast.makeText(this,"Bienvenida (o), has iniciado sesión.",Toast.LENGTH_LONG).show()
                txtResultado.setText("CORRECTAMENTE");
            }else{
                Toast.makeText(this,"Cuidado, verifica tu usuario y/o contraseña.",Toast.LENGTH_LONG).show()
                txtResultado.setText("FALLÓ");
            }
        }

        btnLimpiarUI.setOnClickListener {
            txtUsuarioUI.text.clear()
            txtContrasenaUI.text.clear()
        }
    }
}