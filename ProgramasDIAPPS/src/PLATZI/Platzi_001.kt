/*TEMAS VISTOS
 * NUMEROS PRIMITIVOS Y OBJETOS
 * FOR, RANGOS IF-ELSE
 * WHEN
*/

package Curso

fun main(){

    val a = 324;
    val b = 33;

    println("Operaciones de números con primitivos")
    println(324+33);
    println(324-33);
    println(324*33);
    println(324/33);

    println("\nOperaciones de números con objetos");
    println(a.plus(b)); //SUMAR
    println(a.minus(b)); //RESTAR B Á A
    println(a.times(b)); //MULTIPLICAR
    println(a.div(b)); //DIVIDIR


    
    println("\nUso del FOR");
    //RANGOS
    var oneToHundred = 1..20;
    for(i in 1..20 ){
        print("$i ");
    }
    println();
    for(i in oneToHundred ){
        print("$i ");
    }
    //RANGOS
    var aToZ = 'A'..'Z'
    for(c in 'A'..'Z'){
        print("$c ");
    }
    println()
    for(c in aToZ){
        print("$c ");
    }


    println()
    println("\nUso del IF");
    var numero = 2;
    if(numero.equals(6)){
        println("Son iguales")
    }else{
        println("No son iguales")
    }



    println()
    println("Uso del WHEN (switch)");
    when(numero){
        in 1..5 -> println("Esta entre 1..5") //SI ESTA EN ESE RANGO
        in 1..3 -> println("Esta entre 1..3") //SI ESTA EN ESE RANGO
        !in 1..10 -> println("No esta entre 1..10") //SI NO ESTA ENTRE ESE RANGO
        else -> println("No existe") //SI NINGUNA DE LAS ANTERIORES ESTUVO, ES COMO EL DEFUALT
    }
}
