/*TEMAS VISTOS
 * LAMBDAS
 * ARGUMENTOS NOMBRADOS POR DEFECTO
*/
package Curso
fun main() {
    
 	println("${evaluar('+',2)}")
    println("${evaluar('+')}")
    println("${evaluar()}")
    
    //Lambda - Funciones anónimas, es una funcion que no tiene nombre
    //  ->  la flecha separa los parametros de las instrucciones [parametros] -> [instrucciones o sentencias oacciones]
    var lambda = {a:Int,b:Int -> a*b}(4,2)
    println(lambda)
    
    var lambda2 = {a:Int,b:Int -> a+b}
    
    println(lambda2(34,2))
    
     var saludo = {println("Hola Mundo")}
     saludo()
     
     var plus = {a:Int,b:Int,c:Int -> a+b+c}
     var result = plus(4,4,4)
     println("La suma lambda es $result")
     
     println({a:Int,b:Int,c:Int -> a+b+c}(2,4,2))
     
     var culcularNumero = {n:Int ->
     	when(n){
            in 1..3 -> println("Tu número esta entre 1 y 3")
            in 4..7 -> println("Tu número esta entre 4 y 7")
            in 8..10 -> println("Tu número esta entre 8 y 10")
            else -> println("No se encuentra el número")
        }
     }
     println(culcularNumero(99))
}

//Name Argument(Argumentos nombrados por defecto) - son valores por defecto que se les puede dar a la funcion
fun evaluar(caracter:Char = '=',number:Int = 2):String{
    return "${number} es ${caracter}"
}
