/*TEMAS VISTOS
 * ARREGLOS, METODOS, PRIMITIVOS Y DE OBJETO Y RECORRERLOS
 * EXPRESION ${} Y $
 * FUNCIONES
*/

package Curso

fun main() {
    //ARREGLOS
    //Del mismo tipo, aqui es generica, se abre a cualquier tipo de dato
    val countries = arrayOf("India, México, Colombia, Chile, Perú")

    //Aquí ya se asigna que tipo de dato esta destinada
    //La diferencia de este es que maneja tipos de datos Objetos <String>
    val days = arrayOf<String>("Lunes", "Martes", "Miercoles")

    //Existen métodos que ya tienen su tipo de dato definido
    //La diferencia es que este maneja tipos de datos primitivos <int>
    val numbers = intArrayOf(1, 2, 3, 4, 5, 6, 7, 34, 43, 34, 45, 65)

    //Recorrerlos
    println(countries)

    var sum = 0
    for (num: Int in numbers) {
        sum += num
    }
    val promedio = sum / numbers.size;
    println("El promedio: $promedio")


    var array1 = arrayOf(1, 2, 3, 4, 5)
    var array2: IntArray = array1.toIntArray() //Pasa el arreglo de Objetos a primitivos
    //Los objetos tienen métodos, los primitivos NO

    val suma = array1.sum() //Suma todos los valores del arreglo
    println("La suma de todo el arreglo es: $suma")

    println()
    //Agregar un elemento al arreglo
    array1 = array1.plus(10)
    for (num: Int in array1) {
        print("$num ")
    }
    println()
   
   //Cambia el orden del arreglo siendo el ultimo el primero, y el primero el ultimo
   array1 = array1.reversedArray() //Este método, puedo regresar el arreglo reverso a otro arreglo
   for(num:Int in array1){
       print("$num ")
   }
   println()
   
   array1.reverse() //Y este hace lo mismo pero hacia sí mismo
   for(num:Int in array1){
       print("$num ")
   }
   println()
   println()
   //Usar ${} para expresiones
   //Usar $ para valores
   var x = 5
   println("X es igual a 5?: ${x==5}")
   var mensaje = "El valor de x es: $x"
   x++
   println("${mensaje.replace("es","fue")}, x es igual a: $x")
   
   //FUNCIONES
   //Provistas: Creadas por Kotlin
   //Declaradad: Creadas por nosotros
   println()
   var num:Double = 4.0;
   println("La raíz cuadrada de $num es: ${Math.sqrt(num)}")
   
   var arrayInts = intArrayOf(1,2,3,4,5,3)
   println("El promedio de los números es: ${promedioNumbers(arrayInts)}")
}


fun promedioNumbers(numbers:IntArray): Int{
    var sum = 0
    for(num:Int in numbers){
        sum+=num
    }
    return sum/numbers.size
}
