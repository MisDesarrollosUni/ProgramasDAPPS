package Curso
/* TEMAS VISTOS
 * CLASES
 * SETTERS AND GETTERS CREADOS POR NOSOTROS
 * SETTERS AND GETTERS POR KOTLIN
*/

fun main() {
	
    val camera = Camera()
    camera.turnOn()
    camera.turnOff()
    println("La cámara esta ${camera.getCameraStatus()}")
    
    camera.setResolution(1080);
    println("Resolución: ${camera.getResolution()}")
    
    var shoe = Shoe()
    shoe.size = 44
    println(shoe.size)
    
    shoe.model = "Zapatos"
    println(shoe.model)
}

//SETTERS AND GETTERS POR KOTLIN
class Shoe{
    var size:Int = 34 //Minimo sea 34 y son esto validamos uando se asigne un valor, guardarlo en la variable o no
    	set(value){ // Es una validacion 
            if(value >= 34){
                field = value //field hace referencia a nuestra variable size
            }else{
               field = 34
            }  
        }
        
    var color:String = "White" 
    
    var model:String = "Boots" //NO SE ASIGNE TENIS
    	set(value){
            if(value.equals("Tenis")){
                field = "Boots"
            }else{
                field = value
            }
        }
    
}

//SETTERS AND GETTERS CREADOS POR NOSOTROS
class Camera{
    //TODAS LAS VARIABLES EN LA CLASE SON PUBLICAS POR DEFECTO
    private var isOn: Boolean = false
    private var resolution:Int = 640
    
    
    fun setResolution(resolution:Int){
        this.resolution=resolution
    }
    
    fun getResolution():Int{
        return this.resolution
    }
    
    fun turnOn(){
        isOn = true
    }
    
    fun turnOff(){
        isOn = false
    }
    
    fun getCameraStatus():String{
       return if(isOn) "encendida" else "apagada"
    }
    
   // fun setCameraStatus(onoff:Boolean){
   //   isOn = onoff
   // }
}
