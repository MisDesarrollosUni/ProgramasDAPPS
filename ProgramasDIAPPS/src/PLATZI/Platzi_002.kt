/*TEMAS VISTOS
 * WHILE, DO WHILE
 * NULL SAFETY
 * USO DEL ?, !! , ?:
 * UN POCO DE LISTAS
*/

package Curso

fun main() {
    println("USO DEL WHILE")
    var i = 1
    while (i < 1) {
        println("Mensaje $i")
        i++
    }

    println("USO DEL DO WHILE")
    i = 1
    do {
        println("Mensaje $i")
        i++
    } while (i < 1)

    //NULL SAFETY | Kotlin evita los nulos. ¿Por qué?, ya que Kotlin trabaja con Objetos, estos objetos por
    //por naturaleza son inicializados
    //NPE | Null pointer Exception

    var variableNula: Int? = null;  //PERMITE GUARDAR NULL POR EL CARÁCTER ?
    println(variableNula)

    /* var otraVariableNula:Int = null; // NO PERMITE GUARDAR NULL SIN EL ?
    print(otraVariableNula)
    */


    //Double Bang !!
    // Fuerza a que si se lanze una excepcion y es prefierible no usarlo
    /*var nombreNulo:String?
    nombreNulo = null
    println(nombreNulo!!)
    */


    try {
        var compute: String?
        compute = null;
        var long: Int = compute!!
        println(long)
    } catch (e: Exception) {
        println("Hubo un error ${e}")
    }

    var compute: String? = null
    var long: Int? = compute?.length
    println(long)


    //OPERADOR ELVIS ?:
    //Se sabe que teclado es nulo, entonces con elvis decimos que si es nulo, le asigne el valor posterior
    var teclado: String? = null
    var longitud: Int = teclado?.length ?: 12
    println("Longittud del teclado $longitud")

    //ESta lista puede contener nulos por Int?
    val listWithNulls: List<Int?> = listOf<Int?>(7, null, 4, null, 6)
    println("Lista con nulos ${listWithNulls}}")

    val listWithoutNulls: List<Int> = listWithNulls.filterNotNull()
    println("Lista sin nulos ${listWithoutNulls}")

}
