package Clases

/*
 * Saldaña Espinoza Hector
 * 4C - DSM - UTEZ
 */

//ESTAS SON LAS MATRICES POR DEFECTO
var matrizUno = Array(2, { IntArray(3) })
var matrizDos = Array(3, { IntArray(2) })
var matrizTres = Array(matrizUno.size) { IntArray(matrizDos.get(0).size) }

fun main() {
    var opc: Int = 0
    println("\t***LAS MATRICES PRIMERO DEBEN SER LLENADAS (5)***")
    do {
        try {
            println("MENÚ MATRICES OPERACIONES [${matrizUno.size} x ${matrizUno[0].size}] - [${matrizDos.size} x ${matrizDos[0].size}]")
            println("\t1. Sumar\n\t2. Restar\n\t3. Multiplicar\n\t4. Dividir")
            println("\n\t5. Llenar matrices\n\t6. Imprimir matrices\n\t7. Asignar Matrices\n\t8. Salir\n")
            print("Eliga: ")
            opc = readLine()!!.toInt()
            when (opc) {
                1 -> sumar()
                2 -> restar()
                3 -> multiplicar()
                4 -> dividir()
                5 -> llenarMatrices()
                6 -> imprimirMatrices()
                7 -> asignarMatrices()
                8 -> opc = 8;
                else -> println("*Esa opción no existe")
            }
            println()
            if (opc >= 3 && opc <= 6) {
                println("\tSE HA REALIZADO LA OPERACIÓN CON ÉXITO, IMPRIMA LA MATRIZ")
            }
            println()
        } catch (e: Exception) {
            println("*Ocurrió un problema\n")
        }
    } while (opc != 8);
}

fun asignarMatrices(){
    var indice1:Int = 0
    var indice2:Int = 0
    var indice3:Int = 0
    var indice4:Int = 0
    do {
        print("Indice 1 Matriz 1: ")
        indice1 = readLine()!!.toInt()
        print("Indice 2 Matriz 1: ")
        indice2 = readLine()!!.toInt()
        print("Indice 1 Matriz 2: ")
        indice3 = readLine()!!.toInt()
        print("Indice 2 Matriz 2: ")
        indice4 = readLine()!!.toInt()
        matrizUno = Array(indice1, { IntArray(indice2) })
        matrizDos = Array(indice3, { IntArray(indice4) })
        matrizTres = Array(matrizUno.size) { IntArray(matrizDos.get(0).size) }
        if (indice1 > 0 && indice2 > 0 && indice3 > 0 && indice4 > 0) {
            println("*Ningún valor vacío")
            if(indice2 != indice3){
                println("*Los indices del centro deben ser iguales o, el número de columnas Matriz 1, debe ser el mismo número de filas de Matriz 2")
            }
        }
    } while (indice2 != indice3)
}

fun sumar() {
    for (itUno in 0..matrizUno.size - 1) {
        for (itDos in 0..matrizDos.get(0).size - 1) {
            for (itTres in 0..matrizUno.get(0).size - 1) {
                matrizTres[itUno][itDos] += matrizUno[itUno][itTres] + matrizDos[itTres][itDos]
            }
        }
    }
}

fun restar() {
    for (itUno in 0..matrizUno.size - 1) {
        for (itDos in 0..matrizDos.get(0).size - 1) {
            for (itTres in 0..matrizUno.get(0).size - 1) {
                matrizTres[itUno][itDos] += matrizUno[itUno][itTres] - matrizDos[itTres][itDos]
            }
        }
    }
}

fun dividir() {
    for (itUno in 0..matrizUno.size - 1) {
        for (itDos in 0..matrizDos.get(0).size - 1) {
            for (itTres in 0..matrizUno.get(0).size - 1) {
                matrizTres[itUno][itDos] += matrizUno[itUno][itTres] / matrizDos[itTres][itDos]
            }
        }
    }
}

fun multiplicar() {
    for (itUno in 0..matrizUno.size - 1) {
        for (itDos in 0..matrizDos.get(0).size - 1) {
            for (itTres in 0..matrizUno.get(0).size - 1) {
                matrizTres[itUno][itDos] += matrizUno[itUno][itTres] * matrizDos[itTres][itDos]
            }
        }
    }
}

fun llenarMatrices() {
    println("MATRIZ [${matrizUno.size} x ${matrizUno[0].size}]")
    for (itUno: Int in 0 until matrizUno.size) {
        for (itDos: Int in 0 until matrizUno[itUno].size) {
            print("Número: ")
            matrizUno[itUno][itDos] = readLine()!!.toInt()
        }
    }
    println()
    println("MATRIZ [${matrizDos.size} x ${matrizDos[0].size}]")
    for (itUno: Int in 0 until matrizDos.size) {
        for (itDos: Int in 0 until matrizDos[itUno].size) {
            print("Número: ")
            matrizDos[itUno][itDos] = readLine()!!.toInt()
        }
    }
    println()

}

fun imprimirMatrices() {
    println()
    println("MATRIZ [${matrizUno.size} x ${matrizUno[0].size}]")
    for (itUno: Int in 0 until matrizUno.size) {
        print("[ ")
        for (itDos: Int in 0 until matrizUno[itUno].size) {
            print("${matrizUno[itUno][itDos]} ")
        }
        print("]")
        println()
    }
    println()
    println("MATRIZ [${matrizDos.size} x ${matrizDos[0].size}]")
    for (itUno: Int in 0 until matrizDos.size) {
        print("[ ")
        for (itDos: Int in 0 until matrizDos[itUno].size) {
            print("${matrizDos[itUno][itDos]} ")
        }
        print("]")
        println()
    }
    println()
    println("MATRIZ RESULTADO [${matrizTres.size} x ${matrizTres[0].size}]")
    for (itUno: Int in 0 until matrizTres.size) {
        print("[ ")
        for (itDos: Int in 0 until matrizTres[itUno].size) {
            print("${matrizTres[itUno][itDos]} ")
        }
        print("]")
        println()
    }
    println()
}

