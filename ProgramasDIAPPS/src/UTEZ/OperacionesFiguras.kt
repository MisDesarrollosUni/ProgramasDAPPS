package Clases

/*
 * @author Hector Saldaña Espinoza
 * Desarrollo de Software Multiplataforma
 * Diseño de Apps
 * Figuras
 * 4C UTEZ
 */

class OperacionesFiguras {

    fun validacion(): Int {
        var opc: Int = 0
        var flag: Boolean = false
        do {
            try {
                opc = readLine()!!.toInt()
                flag = true;
            } catch (e: Exception) {
                println("\tSOLO NÚMEROS VÁLIDOS")
                print(": ")
            }
        } while (!flag)
        return opc;
    }

    fun cuadradoConRelleno(tam: Int, carac: String) {
        for (i in 0 until tam) {
            for (j in 0 until tam) {
                print("$carac ")
            }
            println()
        }
        println()
    }

    fun cuadradoSinRelleno(tam: Int, carac: String) {
        for (i in 0 until tam) {
            for (j in 0 until tam) {
                if (j == 0 || j == tam - 1 || i == 0 || i == tam - 1) {
                    print("$carac ")
                } else {
                    print("  ")
                }
            }
            println()
        }
        println()
    }

    fun rectanguloConRelleno(alto: Int, largo: Int, carac: String) {
        for (i in 0 until alto) {
            for (j in 0 until largo) {
                print("$carac ")
            }
            println()
        }
        println()
    }

    fun rectanguloSinRelleno(alto: Int, largo: Int, carac: String) {
        for (i in 0 until alto) {
            for (j in 0 until largo) {
                if (j == 0 || j == largo - 1 || i == 0 || i == alto - 1) {
                    print("$carac ")
                } else {
                    print("  ")
                }
            }
            println()
        }
        println()
    }

    fun trianguloConRelleno(tam: Int, carac: String) {
        var i = 1
        var iter = true
        while (i++ <= tam) {
            iter = i % 2 == 1
            for (j in 1..i) {
                if (!iter) {
                    print("$carac  ")
                } else {
                    print("   ")
                }
                iter = !iter
            }
            println()
        }
        i--
        while (i-- >= 1) {
            iter = i % 2 == 1
            for (j in i downTo 1) {
                if (!iter) {
                    print("$carac  ")
                } else {
                    print("   ")
                }
                iter = !iter
            }
            println()
        }
    }

    fun trianguloSinRelleno(tam: Int, carac: String) {
        var i = 0
        val k: Int
        var match: Int
        i = 1
        while (i <= tam) {
            for (j in 1..i) {
                if (i == j || j == 1 && i % 2 != 0) {
                    print("$carac  ")
                } else {
                    print("   ")
                }
            }
            println()
            i++
        }
        k = i
        match = tam - 1
        i = k
        while (i <= tam + tam - 1) {
            for (j in 1..tam) {
                if (match == j || j == 1 && i % 2 != 0) {
                    print("$carac  ")
                } else {
                    print("   ")
                }
            }
            match--
            println()
            i++
        }
    }

}