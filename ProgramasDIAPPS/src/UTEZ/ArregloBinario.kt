package Clases

fun main() {
    var tamanio:Int = 0
    do{
        println("¿Qué dimensión deseas? (Impar)")
        tamanio = readLine()!!.toInt()
    }while(tamanio%2 == 0)

    var arreglo = IntArray(tamanio)

    //println("Numero base")
    println("Ingresa el numero base")
    var numeroBase:Int= readLine()!!.toInt()
    // pibote, numero base, numero raiz
    arreglo[tamanio/2] = numeroBase

    for(index in 0 until tamanio-1){
        //println("Ingresa un valor")
        println("Ingrese un valor")
        var numero = readLine()!!.toInt()

        // Decision de lado
        if(numero <= arreglo[tamanio/2]){
            for(indexIzquierda in (tamanio/2)-1 downTo 0){
                if(numero >= arreglo[indexIzquierda]){
                    var auxiliar = arreglo[indexIzquierda]
                    arreglo[indexIzquierda] = numero
                    numero = auxiliar
                }
            }
        }else{
            for(indexDerecha in (tamanio/2)+1 until tamanio){
                if(numero <= arreglo[indexDerecha] || arreglo[indexDerecha] == 0){
                    var auxiliar = arreglo[indexDerecha]
                    arreglo[indexDerecha] = numero
                    numero = auxiliar
                }
            }
        }
    }

    // impresión de arreglo
    for(index in 0 until tamanio){
        print("${arreglo[index]} ")
    }
}