/*
 * Saldaña Espinoza Hector
 * Diseño de Apps 4C
 * Examen unidad 1
 */

data class Coche(var placa:String, var modelo:String, var marca:String, var espacio:String)
class SistemaEstacionamiento() {
    private val estacionamiento = Array(3) { BooleanArray(30) }
    private val coches: MutableList<Coche> = ArrayList()
    private var coordenadasCoche = ""

    fun inicio() {
        var opc = 0
        // QUIZA EXAGERE CON ESTO PERO ME GUSTA ESTA LETRA EN MIS PROGRAMAS JEJE
        println(
            "\n" +
                    "███████╗░██████╗████████╗░█████╗░░█████╗░██╗░█████╗░███╗░░██╗░█████╗░███╗░░░███╗██╗███████╗███╗░░██╗████████╗░█████╗░\n" +
                    "██╔════╝██╔════╝╚══██╔══╝██╔══██╗██╔══██╗██║██╔══██╗████╗░██║██╔══██╗████╗░████║██║██╔════╝████╗░██║╚══██╔══╝██╔══██╗\n" +
                    "█████╗░░╚█████╗░░░░██║░░░███████║██║░░╚═╝██║██║░░██║██╔██╗██║███████║██╔████╔██║██║█████╗░░██╔██╗██║░░░██║░░░██║░░██║\n" +
                    "██╔══╝░░░╚═══██╗░░░██║░░░██╔══██║██║░░██╗██║██║░░██║██║╚████║██╔══██║██║╚██╔╝██║██║██╔══╝░░██║╚████║░░░██║░░░██║░░██║\n" +
                    "███████╗██████╔╝░░░██║░░░██║░░██║╚█████╔╝██║╚█████╔╝██║░╚███║██║░░██║██║░╚═╝░██║██║███████╗██║░╚███║░░░██║░░░╚█████╔╝\n" +
                    "╚══════╝╚═════╝░░░░╚═╝░░░╚═╝░░╚═╝░╚════╝░╚═╝░╚════╝░╚═╝░░╚══╝╚═╝░░╚═╝╚═╝░░░░░╚═╝╚═╝╚══════╝╚═╝░░╚══╝░░░╚═╝░░░░╚════╝░"
        )
        do {
            println("\n\t1. Registrár entrada automóvil")
            println("\t2. Salida de automóvil")
            println("\t3. Búscar mi autóvil")
            println("\t4. Salir")
            print("Elige: ")
            opc = readLine()!!.toInt();
            println()
            when (opc) {
                1 -> entrada()
                2 -> salida()
                3 -> {
                    print("Ingresa la placa de tu auto: ")
                    val placaBuscar = readLine()!!.toString()
                    buscarAuto(placaBuscar)
                }
                4 -> {
                    opc = 4
                    println("Esa opción no existe")
                }
                else -> println("Esa opción no existe")
            }
        } while (opc != 4)
    }

    fun salida() {
        var placaBuscar = ""
        print("Ingresa la placa de tu auto: ")
        placaBuscar = readLine()!!.toString()
        if (buscarAuto(placaBuscar)) {
            println("\nHasta la próxima, gracias por venir :)")
        } else {
            println("\nNo se encontró tu coche con placa: $placaBuscar")
        }
    }

    fun buscarAuto(placaBuscar: String): Boolean {
        for (i in coches.indices) {
            if ((coches[i].placa == placaBuscar)) {
                val poscicion = coches[i].espacio.split(",".toRegex()).toTypedArray()
                val x = poscicion[0].toInt()
                val y = poscicion[1].toInt()
                println("\nPiso: " + (x + 1) + " en el lugar: " + (y + 1))
                estacionamiento[x][y] = false
                return true
            }
        }
        println("\nNo se encontró tu coche con placa: $placaBuscar")
        return false
    }

    fun entrada() {
        if (checarEspacio()) {
            print("Ingresa tu placa: ")
            var placaX:String = readLine()!!.toString()
            print("Ingresa tu modelo: ")
            var modeloX:String = readLine()!!.toString()
            print("Ingresa tu marca: ")
            var marcaX:String = readLine()!!.toString()
            var espacioX:String = coordenadasCoche
            val nuevo = Coche(placaX, modeloX, marcaX, espacioX)
            coches.add(nuevo)
        } else {
            println("\nLo sentimos ya no hay lugares disponibles :(")
        }
    }

    fun checarEspacio(): Boolean {
        var i = 0
        var j = 0
        while (i < estacionamiento.size) {
            while (j < estacionamiento[i].size) {
                if (!estacionamiento[i][j]) {
                    estacionamiento[i][j] = true
                    coordenadasCoche = "$i,$j"
                    println("\tEstarás en el piso: " + (i + 1) + " lugar: " + (j + 1))
                    return true
                }
                j++
            }
            i++
        }
        return false
    }
}