package Clases

fun main(args: Array<String>) {

    //3x2
    val matrizA = arrayOf(
            intArrayOf(1, 2, 3),
            intArrayOf(4, 5, 6)
    )
    //2x3
    val matrizB = arrayOf(
            intArrayOf(7, 8),
            intArrayOf(9, 10),
            intArrayOf(11, 12)
    )

    /**
     * Multiplicacion de matrices
     * @param ma Matriz A
     * @param mb Matriz B
     * @return Array Producto de A*B
     */
    fun multiplicar(ma: Array<IntArray>, mb: Array<IntArray>): Array<IntArray> {
        val producto = Array(matrizA.size) { IntArray(matrizB.get(0).size) }

        for (i in 0..matrizA.size - 1) {
            for (j in 0..matrizB.get(0).size - 1) {
                for (k in 0..matrizA.get(0).size - 1) {
                    producto[i][j] += ma[i][k] * mb[k][j]
                }
            }
        }
        return producto
    }

    /**
     * Impresión de matriz
     * @param m Matriz
     */
    fun imprimir(m: Array<IntArray>) {
        for (i: Int in 0..m.size - 1) {
            for (j: Int in 0..m.get(0).size - 1) {
                print(m[i][j])
                print("   ")
            }
            println()
        }
    }

    //----------------------------------------------------------
    println("Matriz A " + matrizA.size + "x" + matrizA.get(0).size)
    imprimir(matrizA);

    println("Matriz B " + matrizB.size + "x" + matrizB.get(0).size)
    imprimir(matrizB);

    if (matrizA.get(0).size == matrizB.size) { //Col A = fila B
        val filA: Int = matrizA.size
        val colB: Int = matrizB.get(0).size
        println("Resultado matriz $filA x $colB")
        val resultado = multiplicar(matrizA, matrizB);
        imprimir(resultado);
    } else {
        println("No se puede multiplicar")
    }

}