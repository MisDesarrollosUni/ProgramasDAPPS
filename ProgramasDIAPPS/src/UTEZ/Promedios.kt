package Clases

/* Saldaña Espinoza Hector
 * 4C UTEZ
 */

fun main() {
    data class Estudiante(var nombreCompleto: String, var grupo: String, var promedio: Double)
    var lista = mutableListOf<Estudiante>();
    var numEstudintes: Int
    var nombre: String
    var grupo: String
    var promedio: Double = 0.0
    var suma: Double = 0.0
    var calif:Double = 0.0
    var promedioGeneral: Double = 0.0
    print("¿Cuántos Estudiantes?: ")
    numEstudintes = readLine()!!.toInt()
    for (iter: Int in 0 until numEstudintes) {
        println("\tESTUDIANTE ${iter + 1}")
        print("Nombre Completo: ")
        nombre = readLine()!!.toString()
        print("Grupo: ")
        grupo = readLine().toString()
        for (iterCal: Int in 1..5) {
            print("\tCalificación ${iterCal}: ")
            calif = readLine()!!.toDouble()
            suma += calif
        }
        promedio = suma/5
        var estudiante  = Estudiante(nombre,grupo,promedio)
        lista.add(estudiante)
        promedioGeneral += promedio
        promedio = 0.0
        calif = 0.0
        suma = 0.0
        println()
    }
    promedioGeneral /= numEstudintes;
    for(iter:Int in 0 until lista.size){
        println("\n\tESTUDIANTE ${iter + 1}")
        println("Nombre Completo: ${lista[iter].nombreCompleto}")
        println("Grupo: ${lista[iter].grupo}")
        println("Promedio: ${lista[iter].promedio}")
    }
    println("\n\tPromedio General: $promedioGeneral")

}
