package Clases

/*
 * TEMAS VISTOS
 * IF
 * WHEN (SWITCH)
 * LISTAS
*/

fun main() {
    var num: Int = 4

    when (num) {
        1 -> println("El número es 1")
        2 -> println("El número es 2")
        3 -> println("El número es 3")
        4 -> {
            println("La suma es ${num + 65}") //SI HAY MAS LINEAS SE USA LAMBDA
        }
        else -> println("Valor no encontrado")
    }

    //Iterable - iterator     //Esto es un arreglo 0..10
    for (index: Int in 0..10) {
        println(index) // TOMA 0 y 10
    }

    for (index: Int in 0 until 10) {
        println(index) //UNTIL NO TOMA EL FINAL
    }

    for (index: Int in 10 downTo 0) {
        println(index) //Decrementa
    }

    //mutableListOf es para tener una lista dinamica
    //ListOf es solo valores estaticos

    //Lista de String
    var listaM = mutableListOf<String>("Hola", "Adios", "GG")

    //Lista generica
    var lista = mutableListOf("Hola", "Adios", "GG", 2323, 3.5, false)
    lista.add("Otro elemento")

    //Los recorre, y withIndex() trae su poscicion
    //primero indice y valor
    for ((index, valor) in lista.withIndex()) {
        println("-> $index, valor ${valor}")
    }

    data class ElementoX(var elemento: String, var id: Int)

    var elemento1 = ElementoX("Elemento 1", 1)
    var elemento2 = ElementoX("Elemento 2", 2)
    var elemento3 = ElementoX("Elemento 3", 3)

    var listaClase = mutableListOf<ElementoX>()
    listaClase.add(elemento1)
    listaClase.add(elemento2)
    listaClase.add(elemento3)

    for ((index, elemento) in listaClase.withIndex()) {
        println("${elemento.elemento} y su index: $index")
    }

}



