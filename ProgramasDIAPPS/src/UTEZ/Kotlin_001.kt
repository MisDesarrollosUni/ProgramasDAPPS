package Clases


/* TEMAS VISTOS
 * CONSTRUCTOR
 * HERENCIA
 * CLASE PADRE E HIJA
*/

fun main() {

    var coche=Terrestre()
    coche.avanzar()


}

//CON OPEN ACCESAMOS A LAS CLASES
//SI NO COLOCAMOS OPEN, POR DEFULT ES FINAL Y NO PODEMOS ACCEDER A LA CLASE
open class Transporte(){
    private var color:String="Azul"
    open var velocidadMaxima:Double =0.0

    //OPEN ES DECIR QUE SE ESTA USANDO EN OTRA CLASE COMO ESCRITURA
    open fun avanzar(){
        println("Avanzar como Transporte")
    }

    fun frenar(){
        println("Frenar como Transporte")
    }

    fun setColor(color:String){
        this.color = color
    }
    fun getColor():String{
        return color
    }

}

class Terrestre:Transporte(){

    override var velocidadMaxima:Double = 180.0

    //OVERRIDE DECIMOS QUE ESTE METODO ES SOBREESCRITO
    override fun avanzar(){
        println("Avanzar como Terrestre a $velocidadMaxima")
    }

}
class Aereo:Transporte(){

}