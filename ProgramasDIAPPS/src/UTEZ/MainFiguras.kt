package Clases

/*
 * @author Hector Saldaña Espinoza
 * Desarrollo de Software Multiplataforma
 * Diseño de Apps
 * Figuras
 * 4C UTEZ
 */


fun main() {
    var op = OperacionesFiguras()
    var carac: String = ""
    var tam: Int = 0
    var largo: Int = 0
    var alto: Int = 0
    var opc: Int = 0
    do {
        println("___________________________")
        println("\tMENÚ PINTAR FIGURAS")
        println("1.- Cuadrado con Relleno")
        println("2.- Cuadrado sin Relleno\n")
        println("3.- Triángulo con Relleno")
        println("4.- Triángulo sin Relleno\n")
        println("5.- Rectángulo con Relleno")
        println("6.- Rectángulo sin Relleno\n")
        println("7.- Salir\n")
        print("Elegi: ")
        opc = op.validacion()
        when (opc) {
            in 1..2 -> {
                print("Tamaño: ")
                tam = op.validacion()
                print("Carácter: ")
                carac = readLine()!!.toString()
            }
            in 3..4 -> {
                print("Tamaño: ")
                tam = op.validacion()
                print("Carácter: ")
                carac = readLine()!!.toString()
            }
            in 5..6 -> {
                print("Largo: ")
                largo = op.validacion()
                print("Alto: ")
                alto = op.validacion()
                print("Carácter: ")
                carac = readLine()!!.toString()
            }
        }
        println()
        when (opc) {
            1 -> op.cuadradoConRelleno(tam, carac)
            2 -> op.cuadradoSinRelleno(tam, carac)
            3 -> op.trianguloConRelleno(tam, carac)
            4 -> op.trianguloSinRelleno(tam, carac)
            5 -> op.rectanguloConRelleno(alto, largo, carac)
            6 -> op.rectanguloSinRelleno(alto, largo, carac)
            7 -> {
                println("\tPROGRAMA FINALIZADO")
                opc = 7
            }
            else -> println("OPCIÓN INVÁLIDA")
        }
    } while (opc != 7)

}