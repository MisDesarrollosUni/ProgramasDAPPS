package Clases

/* TEMAS VISTOS
 * ACCEDER A UN ELEMENTO DE LA LITSA
 * MAPAS
 * ?, !!, el ? verifica si es null, si es null no imprime
 * ARRAYS
*/


fun main() {
    val list = mutableListOf("Hola", "adios", "bebe")
    println(list[0])
    println(list.get(0))

    val map = mutableMapOf<String,String>("ID0" to "VALOR ID 0")
    map.set("ID1","VALOR ID 1")
    map.set("ID2", "VALOR ID 2")
    map.set("ID3","VALOR ID 3")

    println(map.get("ID0"))
    println(map["ID1"])

    var arrayNumeros = IntArray(10)
    arrayNumeros[5]  = 100
    println(arrayNumeros[5])

    //INICIALIZADOR A TODAS LAS POSCI, SOLO LOS STRING
    var arrayString = Array<String>(10){"Hola a todos"}
    println(arrayString[2])

    //ARREGLO BIDIMENSIONAL
    var arregloBidiNums = Array(5, {IntArray(10)})
    arregloBidiNums[1][8] = 52
    println(arregloBidiNums[1][8])

    var arregloMultiNums = Array(2,{Array(2,{IntArray(10)})})
    arregloMultiNums[0][0][0] = 844
    println(arregloMultiNums[0][0][0])
}