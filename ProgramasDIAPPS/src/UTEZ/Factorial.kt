package Clases

/* Saldaña Espinoza Hector
 * 4C UTEZ
 */
fun main() {
    var factorial: Long = 1
    var num: Int = 0
    var cadena: String = ""
    print("Ingresa un número: ")
    num = readLine()!!.toInt()
    cadena += "$num!: "
    for (iter in 1..num) {
        factorial *= iter.toLong()
    }
    for (iter in num downTo 1) {
        if (iter != 1)
            cadena += "$iter x "
        else
            cadena += "$iter"
    }
    println("$cadena = $factorial")
}
