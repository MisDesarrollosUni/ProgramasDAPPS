
package JAVA.Repaso3;

/**
 *
 * @author Hector Saldaña
 */
public class Barco extends Maritimo{
    private String tipoMotor;
    @Override
    public void avanzar(){
        System.out.println("Barco | avanzar");
    }
    
    @Override
    public void frenar(){
         System.out.println("Barco | frenar");
    }
}
