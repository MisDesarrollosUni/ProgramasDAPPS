
package JAVA.Repaso3;

/**
 *
 * @author Hector Saldaña
 */
public class Maritimo extends Transporte {
    
    @Override
    public void avanzar(){
        System.out.println("Maritimo | avanzar");
    }
    
    @Override
    public void frenar(){
         System.out.println("Maritimo | frenar");
    }
    
}
