
package JAVA.Repaso3;

/**
 *
 * @author Hector Saldaña
 */
public class Avioneta extends Aereo{
    @Override
    public void avanzar(){
        System.out.println("Avioneta | avanzar");
    }
    
    @Override
    public void frenar(){
        System.out.println("Avioneta | frenar");
    }
    
    @Override
    public void girarSobreEje(){
        System.out.println("Avioneta | girar sobre eje");
    }
    
}
