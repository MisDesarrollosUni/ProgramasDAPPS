
package JAVA.Repaso3;

/**
 *
 * @author Hector Saldaña
 */
public class Coche extends Terrestre{
    @Override
    public void avanzar(){
        System.out.println("Coche | avanzar");
    }
    
    @Override
   public void encenderMotor(){
        System.out.println("Coche | encender motor");
    }
   
    @Override
   public void frenar(){
        System.out.println("Coche | frenar");
    }
}
