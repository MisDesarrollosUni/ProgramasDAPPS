
package JAVA.Repaso3;

/**
 *
 * @author Hector Saldaña
 */
public class Aereo extends Transporte {
    private int numeroAlerones;
    
    @Override
    public void avanzar(){
        System.out.println("Aereo | avanzar");
    }
    
    @Override
    public void frenar(){
     System.out.println("Aereo | frenar");   
    }
    
    public void girarSobreEje(){
        System.out.println("Aereo | girar sobre eje");
    }
    
}
