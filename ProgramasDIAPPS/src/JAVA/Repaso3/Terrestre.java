
package JAVA.Repaso3;

/**
 *
 * @author Hector Saldaña
 */
public class Terrestre extends Transporte {
    private int numeroLlantas;
    private String tipoDireccion;
    
    @Override
    public void avanzar(){
        System.out.println("Terrestre | avanzar");
    }
    
    public void encenderMotor(){
        System.out.println("Terrestre | encender motor");
    }
    
    @Override
    public void frenar(){
        System.out.println("Tarrestre | frenar");
    }
}
