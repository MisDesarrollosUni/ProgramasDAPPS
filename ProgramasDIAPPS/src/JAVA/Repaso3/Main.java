package JAVA.Repaso3;
/**
 *
 * @author Hector Saldaña
 */
public class Main {

    public static void main(String[] args) {
        Repaso3.Transporte trans = new Repaso3.Transporte();
        trans.avanzar();
        trans.frenar();

        System.out.println("");

        Repaso3.Terrestre terres = new Repaso3.Terrestre();
        terres.avanzar();
        terres.encenderMotor();
        terres.frenar();

        System.out.println("");

        Repaso3.Maritimo mar = new Repaso3.Maritimo();
        mar.avanzar();
        mar.frenar();

        System.out.println("");

        Repaso3.Aereo ar = new Repaso3.Aereo();
        ar.avanzar();
        ar.girarSobreEje();
        ar.frenar();

        System.out.println("");

        Repaso3.Coche co = new Repaso3.Coche();
        co.avanzar();
        co.encenderMotor();
        co.frenar();

        System.out.println("");
        Repaso3.Barco ba = new Repaso3.Barco();
        ba.avanzar();
        ba.frenar();

        System.out.println("");

        Repaso3.Avioneta av = new Repaso3.Avioneta();
        av.avanzar();
        av.frenar();
        av.girarSobreEje();

    }
}
