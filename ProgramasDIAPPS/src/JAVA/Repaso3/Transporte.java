
package JAVA.Repaso3;

/**
 *
 * @author Hector Saldaña
 */
public class Transporte {
    protected String color;
    private double velocidadMaxima;
    
    public void avanzar(){
        System.out.println("Transporte | avanzar");
    }
    
    public void frenar(){
        System.out.println("Transporte | frenar");
    }
}
