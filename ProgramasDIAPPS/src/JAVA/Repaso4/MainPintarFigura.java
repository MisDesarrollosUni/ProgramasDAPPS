package JAVA.Repaso4;

import java.util.Scanner;

/**
 *
 * @author Hector Saldaña
 */
public class MainPintarFigura {

    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        String caracter = "";
        int limite=0;
        System.out.print("# de lados: ");
        limite = leer.nextInt();
        System.out.print("Carácter: ");
        caracter = leer.next();
        
        
        for (int i = 0; i < limite; i++) {
            for (int j = 0; j < limite; j++) {
                if (j == 0 || j == limite-1|| i == 0 || i == limite-1) {
                    System.out.print(caracter+" ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
}
