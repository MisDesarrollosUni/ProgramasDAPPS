
package JAVA.Repaso1;

import java.util.Scanner;

public class MainParImparPrimos {

    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        int nums, numero, pares = 0, impares = 0, primos = 0, divisible = 0;
        System.out.print("¿Cuántos números?: ");
        nums = leer.nextInt();
        for (int i = 1; i <= nums; i++) {
            System.out.print("Ingresa valor: ");
            numero = leer.nextInt();
            if (numero % 2 == 0) {
                pares++;
            } else {
                for (int j = 1; j <= numero; j++) {
                    if (numero % i == 0) {
                        divisible++;
                        if (divisible >= 3) {
                            break;
                        }
                    }
                }
                if (divisible > 2) {
                    impares++;
                } else {
                    primos++;
                    impares++;
                }
            }
        }
        System.out.println("Pares: " + pares);
        System.out.println("Impares: " + impares);
        System.out.println("Primos: " + primos);
    }
}
