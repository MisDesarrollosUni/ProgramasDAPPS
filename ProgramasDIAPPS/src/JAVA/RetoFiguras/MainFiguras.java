package JAVA.RetoFiguras;

import java.util.Scanner;

/**
 * @author Hector Saldaña Espinoza
 * Desarrollo de Software Multiplataforma
 * Diseño de Apps
 * Repaso de Java
 * 4C UTEZ
 */
public class MainFiguras {
    
    public static void main(String[] args) {
        RetoFiguras.OperacionesFiguras op = new RetoFiguras.OperacionesFiguras();
        Scanner leer = new Scanner(System.in);
        String carac = "";
        int tam = 0, largo = 0, alto = 0, opc = 0;
        do {
            System.out.println("___________________________");
            System.out.println("\tMENÚ PINTAR FIGURAS");          
            System.out.println("1.- Cuadrado con Relleno");
            System.out.println("2.- Cuadrado sin Relleno\n");           
            System.out.println("3.- Triángulo con Relleno");
            System.out.println("4.- Triángulo sin Relleno\n");            
            System.out.println("5.- Rectángulo con Relleno");
            System.out.println("6.- Rectángulo sin Relleno\n");           
            System.out.println("7.- Salir\n");
            opc = op.elegirOpcion();
            switch (opc) {
                case 1:
                case 2:
                    System.out.print("Tamaño: ");
                    tam = op.validarNumero();
                    System.out.print("Carácter: ");
                    carac = leer.next();
                    break;
                case 3:
                case 4:
                    System.out.print("Tamaño: ");
                    tam = op.validarNumero();
                    System.out.print("Carácter: ");
                    carac = leer.next();
                    break;               
                case 5:
                case 6:
                    System.out.print("Largo: ");
                    largo = op.validarNumero();
                    System.out.print("Alto: ");
                    alto = op.validarNumero();
                    System.out.print("Carácter: ");
                    carac = leer.next();
                    break;        
            }
            System.out.println();
            switch (opc) {
                case 1:
                    op.cuadradoConRelleno(tam, carac);
                    break;
                case 2:
                    op.cuadradoSinRelleno(tam, carac);
                    break;
                case 3:
                    op.trianguloConRelleno(tam, carac);
                    break;
                case 4:
                    op.trianguloSinRelleno(tam, carac);
                    break;
                case 5:
                    op.rectanguloConRelleno(alto, largo, carac);
                    break;
                case 6:
                    op.rectanguloSinRelleno(alto, largo, carac);
                    break;
                case 7:
                    System.out.println("\tPROGRAMA FINALIZADO");
                    opc = 7;
                    break;                
                default:
                    System.out.println("OPCIÓN INVÁLIDA");
            }
        
        } while (opc != 7);
        
    }
    
}
