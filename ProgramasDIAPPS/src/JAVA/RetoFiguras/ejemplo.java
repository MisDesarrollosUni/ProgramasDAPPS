/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JAVA.RetoFiguras;

/**
 *
 * @author Hector Saldaña
 */
public class ejemplo {

    final static String ASTERISCO = "*";
    final static String ESPACIO = " ";
    final static int ROWS = 5;

    public static void main(String[] args) {

        trianguloIncrementa(ASTERISCO, ESPACIO); //genera a)
        System.out.println();
        trianguloDecrementa(ESPACIO, ASTERISCO); // genera b)
        System.out.println();
        trianguloIncrementa(ESPACIO, ASTERISCO); // genera c)
        System.out.println();
        trianguloDecrementa(ASTERISCO, ESPACIO); // genera d)
        System.out.println();
        trianguloPiramide(ESPACIO, ASTERISCO); // genera e)
    }

    public static void trianguloIncrementa(String primero, String segundo) {
        for (int i = ROWS; i > -1; i--) {
            for (int j = ROWS; j > i; j--) {
                System.out.print(primero);
            }
            for (int j = 1; j < i + 1; j++) {
                System.out.print(segundo);
            }
            System.out.println();
        }
    }

    public static void trianguloDecrementa(String primero, String segundo) {
        for (int i = ROWS; i > -1; i--) {
            for (int j = 1; j < i + 1; j++) {
                System.out.print(primero);
            }
            for (int j = ROWS; j > i; j--) {
                System.out.print(segundo);
            }
            System.out.println();
        }
    }

    private static void trianguloPiramide(String primero, String segundo) {
        for (int i = ROWS; i > -1; i--) {
            for (int j = 1; j < i + 1; j++) {
                System.out.print(primero);
            }
            for (int j = ROWS; j > i; j--) {
                System.out.print(segundo);
            }
            if (i < ROWS - 1) {
                for (int j = ROWS - 1; j > i; j--) {
                    System.out.print(segundo);
                }
            }
            System.out.println();
        }
    }
}
