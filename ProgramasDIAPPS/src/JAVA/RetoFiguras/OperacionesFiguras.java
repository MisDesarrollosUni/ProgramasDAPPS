package JAVA.RetoFiguras;

import java.util.Scanner;

/**
 * @author Hector Saldaña Espinoza
 * Desarrollo de Software Multiplataforma
 * Diseño de Apps
 * Repaso de Java
 * 4C UTEZ
 */
public class OperacionesFiguras {

    Scanner leer = new Scanner(System.in);

    public int elegirOpcion() {
        int opc = 0;
        boolean flag = false;
        do {
            try {
                System.out.print("Eliga: ");
                opc = leer.nextInt();
                flag = true;
            } catch (Exception e) {
                leer.next();
            }
        } while (!flag);

        return opc;
    }

    public int validarNumero(){
        int numero = 0;
        boolean flag = false;
        do{
            try{
                numero = leer.nextInt();
                flag = true;
            }catch(Exception e){
                System.out.println("Solo números");
                System.out.print(": ");
                leer.next();
            }
        }while(!flag);
        return numero;
    }
    
    public void cuadradoConRelleno(int tam, String carac) {
        for (int i = 0; i < tam; i++) {
            for (int j = 0; j < tam; j++) {
                System.out.print(carac + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public void cuadradoSinRelleno(int tam, String carac) {
        for (int i = 0; i < tam; i++) {
            for (int j = 0; j < tam; j++) {
                if (j == 0 || j == tam - 1 || i == 0 || i == tam - 1) {
                    System.out.print(carac + " ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    public void rectanguloConRelleno(int alto, int largo, String carac) {
        for (int i = 0; i < alto; i++) {
            for (int j = 0; j < largo; j++) {
                System.out.print(carac + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public void rectanguloSinRelleno(int alto, int largo, String carac) {
        for (int i = 0; i < alto; i++) {
            for (int j = 0; j < largo; j++) {
                if (j == 0 || j == largo - 1 || i == 0 || i == alto - 1) {
                    System.out.print(carac + " ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    public void trianguloConRelleno(int tam, String carac) {
        int i = 1;
        boolean iter = true;
        while (i++ <= tam) {
            iter = i % 2 == 1;
            for (int j = 1; j <= i; j++) {
                if (!iter) {
                    System.out.print(carac + "  ");
                } else {
                    System.out.print("   ");
                }
                iter = !iter;
            }
            System.out.println();
        }
        i--;
        while (i-- >= 1) {
            iter = i % 2 == 1;
            for (int j = i; j > 0; j--) {
                if (!iter) {
                    System.out.print(carac + "  ");
                } else {
                    System.out.print("   ");
                }
                iter = !iter;
            }
            System.out.println();
        }
    }

    public void trianguloSinRelleno(int tam, String carac) {
        int i = 0, k, match;
        for (i = 1; i <= tam; i++) {
            for (int j = 1; j <= i; j++) {
                if (i == j || (j == 1) && i % 2 != 0) {
                    System.out.print(carac + "  ");
                } else {
                    System.out.print("   ");
                }
            }
            System.out.println();
        }
        k = i;
        match = tam - 1;
        for (i = k; i <= (tam + tam - 1); i++) {
            for (int j = 1; j <= tam; j++) {
                if (match == j || j == 1 && i % 2 != 0) {
                    System.out.print(carac + "  ");
                } else {
                    System.out.print("   ");
                }
            }
            match--;
            System.out.println();
        }
    }

}
