package JAVA.Repaso5;

import java.util.Scanner;

/**
 *
 * @author Hector Saldaña
 */
public class MainArreglo {

    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        int[][] matriz1 = new int[3][3];
        int[][] matriz2 = new int[3][3];
        int[][] matrizResultado = new int[3][3];
        int opc = 0;
        do {
            System.out.println("1.- Llenar matrices");
            System.out.println("2.- Mostrar matrices");
            System.out.println("3. Suma");
            System.out.println("4. Resta");
            System.out.println("5. Multiplicacion");
            System.out.println("6. Division");
            System.out.println("7. Salir");
            opc = leer.nextInt();
            switch (opc) {
                case 1:
                    System.out.println("Primera Matriz");
                    for (int i = 0; i < matriz1.length; i++) {
                        for (int j = 0; j < matriz1[i].length; j++) {
                            matriz1[i][j] = leer.nextInt();
                        }
                    }
                    System.out.println("Segunda Matriz");
                    for (int i = 0; i < matriz1.length; i++) {
                        for (int j = 0; j < matriz1[i].length; j++) {
                            matriz2[i][j] = leer.nextInt();
                        }
                    }
                    break;

                case 2:
                    for (int i = 0; i < matriz1.length; i++) {
                        System.out.print("[");
                        for (int j = 0; j < matriz1[i].length; j++) {
                            System.out.print(matriz1[i][j]+" ");
                        }
                        System.out.println("]");
                    }
                    System.out.println();
                    for (int i = 0; i < matriz1.length; i++) {
                        System.out.print("[");
                        for (int j = 0; j < matriz1[i].length; j++) {
                            System.out.print(matriz2[i][j]+" ");
                        }
                        System.out.println("]");
                    }
                    break;

                case 3:
                    for (int i = 0; i < matrizResultado.length; i++) {
                        for (int j = 0; j < matrizResultado[i].length; j++) {
                            matrizResultado[i][j] = matriz1[i][j] + matriz2[i][j];
                        }
                    }
                    System.out.println();
                    for (int i = 0; i < matriz1.length; i++) {
                        System.out.print("[");
                        for (int j = 0; j < matriz1[i].length; j++) {
                            System.out.print(matriz2[i][j]+" ");
                        }
                        System.out.println("]");
                    }
                    break;
                case 4:
                    break;
                case 5:
                    break;
                case 6:
                    break;

                default:
                    System.out.println("INVALIDO");
            }
        } while (opc != 7);

    }
}
