package JAVA.Repaso2;

import java.util.Scanner;

/**
 *
 * @author Hector Saldaña
 */
public class MainSerie {

    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        int tNum = 0, iNum;
        String originales = "[1]", faltantes = "[ ]", ingresados = "[ ]";
        boolean flag = false;
        System.out.println("\tSERIE DE NÚMEROS DEL 1 AL N\n\t(SIN CONTAR 1 Y N)");
        do {
            try {
                System.out.print("Número límite (N): ");
                tNum = leer.nextInt();
                if (tNum > 1) 
                    flag = true;               
            } catch (Exception e) {
                System.out.println("Solo números y mayores a 1");
                leer.next();
            }
        } while (!flag);
        for (int i = 2; i < tNum; i++) {
            flag = false;
            do {
                try {
                    System.out.print(": ");
                    iNum = leer.nextInt();
                    if (iNum == i) 
                        faltantes += "[ ]";
                     else 
                        faltantes += "[" + i + "]";                 
                    ingresados += "[" + iNum + "]";
                    originales += "[" + i + "]";
                    flag = true;
                } catch (Exception e) {
                    System.out.println("Solo números");
                    leer.next();
                }
            } while (!flag);
        }
        originales += "[" + tNum + "]";
        faltantes += "[ ]";
        ingresados += "[ ]";
        System.out.println("\nNúmeros originales: " + originales);
        System.out.println("\nNúmeros  faltantes: " + faltantes);
        System.out.println("\nNúmeros ingresados: " + ingresados);
    }
}
