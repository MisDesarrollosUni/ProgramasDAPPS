package mx.edu.utez.agenciadecoches

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import mx.edu.utez.agenciadecoches.databinding.ActivityInterioresBinding
import java.lang.Exception

class InterioresActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityInterioresBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val operaciones = Operaciones()
        operaciones.vaciarLista()
        operaciones.iniciarInteriores()

        var seleccionado = IntArray(3)

        var guardadoUsuario:ArrayList<AccesorioBean> = ArrayList();

        var adapStereo: ArrayAdapter<String> = ArrayAdapter(this, R.layout.spn_elemento, operaciones.retornarStereo())
        adapStereo.setDropDownViewResource(R.layout.spn_elementos)
        binding.spnStereo.adapter = adapStereo

        var adapMaterial: ArrayAdapter<String> = ArrayAdapter(this, R.layout.spn_elemento, operaciones.retornarMaterial())
        adapMaterial.setDropDownViewResource(R.layout.spn_elementos)
        binding.spnMaterial.adapter = adapMaterial

        var adapVidrios: ArrayAdapter<String> = ArrayAdapter(this, R.layout.spn_elemento, operaciones.retornarVidrios())
        adapVidrios.setDropDownViewResource(R.layout.spn_elementos)
        binding.spnVidrios.adapter = adapVidrios

        binding.spnStereo.onItemSelectedListener=object:AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { TODO("Not yet implemented") }

            override fun onNothingSelected(parent: AdapterView<*>?) { TODO("Not yet implemented") }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { seleccionado[0] = position }
        }

        binding.spnMaterial.onItemSelectedListener =object :AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { TODO("Not yet implemented") }

            override fun onNothingSelected(parent: AdapterView<*>?) { TODO("Not yet implemented") }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { seleccionado[1] = position+3 }
        }

        binding.spnVidrios.onItemSelectedListener=object :AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { TODO("Not yet implemented") }

            override fun onNothingSelected(parent: AdapterView<*>?) { TODO("Not yet implemented") }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { seleccionado[2] = position+6 }
        }

        binding.btnSiguiente.setOnClickListener {
            var ventana = AlertDialog.Builder(this)
            ventana.setTitle("¿Pasar a las siguientes categorías?")
            ventana.setMessage("Tu personalización quedará guardada")
            ventana.setPositiveButton("Si"){ ventana, id ->
                try{
                    for ( i in 0 until 3)
                        guardadoUsuario.add(operaciones.accesoriosInteriores.get(seleccionado[i]))
                    val pantallaExteriores = Intent(this, ExterioresActivity::class.java)
                    pantallaExteriores.putExtra("guardados",guardadoUsuario)
                    startActivity(pantallaExteriores)
                }catch (e:Exception){
                    println(e)
                }
            }
            ventana.setNegativeButton("No"){ventana, id->}
            ventana.show()
        }

    }

}
