package mx.edu.utez.agenciadecoches

class Operaciones {
    val accesoriosInteriores:ArrayList<AccesorioBean> = ArrayList<AccesorioBean>()

    fun vaciarLista(){
        accesoriosInteriores.clear()
    }

    fun iniciarInteriores(){
        accesoriosInteriores.add(AccesorioBean("Sony",3500.0))
        accesoriosInteriores.add(AccesorioBean("Kenwood",7500.0))
        accesoriosInteriores.add(AccesorioBean("Pioneer",5000.0))

        accesoriosInteriores.add(AccesorioBean("Piel",10000.0))
        accesoriosInteriores.add(AccesorioBean("Plástico",3800.0))
        accesoriosInteriores.add(AccesorioBean("Madera",25000.0))

        accesoriosInteriores.add(AccesorioBean("2 puertas",7000.0))
        accesoriosInteriores.add(AccesorioBean("4 puertas",12000.0))
    }

    fun iniciarExteriores(){
        accesoriosInteriores.add(AccesorioBean("Aluminio",7500.0))
        accesoriosInteriores.add(AccesorioBean("Metal",4000.0))

        accesoriosInteriores.add(AccesorioBean("Fibra de carbón",3500.0))
        accesoriosInteriores.add(AccesorioBean("Led",6300.0))
        accesoriosInteriores.add(AccesorioBean("Xenón",4500.0))

        accesoriosInteriores.add(AccesorioBean("Paquete 1",3500.0))
        accesoriosInteriores.add(AccesorioBean("Paquete 2",5000.0))
    }

    fun iniciarAccesorios(){
        accesoriosInteriores.add(AccesorioBean("Eléctrico",8500.0))
        accesoriosInteriores.add(AccesorioBean("Manual",4000.0))

        accesoriosInteriores.add(AccesorioBean("Frontales",6000.0))
        accesoriosInteriores.add(AccesorioBean("Completo",12500.0))

        accesoriosInteriores.add(AccesorioBean("Frontales",5200.0))
        accesoriosInteriores.add(AccesorioBean("Completo",8400.0))
    }

    fun retornarQuemacocos():ArrayList<String>{
        var elementos: ArrayList<String> = ArrayList();
        for (i in 0..1) elementos.add(accesoriosInteriores.get(i).nombre)
        return elementos
    }

    fun retornarBolsas():ArrayList<String>{
        var elementos: ArrayList<String> = ArrayList();
        for (i in 2..3) elementos.add(accesoriosInteriores.get(i).nombre)
        return elementos
    }

    fun retornarAire():ArrayList<String>{
        var elementos: ArrayList<String> = ArrayList();
        for (i in 4..5) elementos.add(accesoriosInteriores.get(i).nombre)
        return elementos
    }


    // ------------------------------------------------------
    fun retornarRines():ArrayList<String>{
        var elementos: ArrayList<String> = ArrayList();
        for (i in 0..1) elementos.add(accesoriosInteriores.get(i).nombre)
        return elementos
    }

    fun retornarFaros():ArrayList<String>{
        var elementos: ArrayList<String> = ArrayList();
        for (i in 2..4) elementos.add(accesoriosInteriores.get(i).nombre)
        return elementos
    }

    fun retornarKit():ArrayList<String>{
        var elementos: ArrayList<String> = ArrayList();
        for (i in 5..6) elementos.add(accesoriosInteriores.get(i).nombre)
        return elementos
    }

// ------------------------------------------------------
    fun retornarStereo(): ArrayList<String>{
        var elementos: ArrayList<String> = ArrayList();
        for (i in 0..2) elementos.add(accesoriosInteriores.get(i).nombre)
        return elementos
    }

    fun retornarMaterial():ArrayList<String>{
        var elementos: ArrayList<String> = ArrayList();
        for (i in 3..5) elementos.add(accesoriosInteriores.get(i).nombre)
        return elementos
    }

    fun retornarVidrios():ArrayList<String>{
        var elementos: ArrayList<String> = ArrayList();
        for (i in 6..7) elementos.add(accesoriosInteriores.get(i).nombre)
        return elementos
    }

}