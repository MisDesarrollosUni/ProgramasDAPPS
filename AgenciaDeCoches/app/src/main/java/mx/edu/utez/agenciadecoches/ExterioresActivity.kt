package mx.edu.utez.agenciadecoches
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.ArrayAdapter
import mx.edu.utez.agenciadecoches.databinding.ActivityExterioresBinding
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import java.lang.Exception

class ExterioresActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityExterioresBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val operaciones = Operaciones()
        operaciones.vaciarLista()
        operaciones.iniciarExteriores()
        var seleccionado = IntArray(3)

        var guardadoUsuario = intent.getSerializableExtra("guardados") as? ArrayList<AccesorioBean>

        var adapRines: ArrayAdapter<String> = ArrayAdapter(this, R.layout.spn_elemento, operaciones.retornarRines())
        adapRines.setDropDownViewResource(R.layout.spn_elementos)
        binding.spnRines.adapter = adapRines

        var adapFaros: ArrayAdapter<String> = ArrayAdapter(this, R.layout.spn_elemento, operaciones.retornarFaros())
        adapFaros.setDropDownViewResource(R.layout.spn_elementos)
        binding.spnFaros.adapter = adapFaros

        var adapKit: ArrayAdapter<String> = ArrayAdapter(this, R.layout.spn_elemento, operaciones.retornarKit())
        adapKit.setDropDownViewResource(R.layout.spn_elementos)
        binding.spnKit.adapter = adapKit

        binding.spnRines.onItemSelectedListener=object: AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { TODO("Not yet implemented") }

            override fun onNothingSelected(parent: AdapterView<*>?) { TODO("Not yet implemented") }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { seleccionado[0] = position }
        }

        binding.spnFaros.onItemSelectedListener=object: AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { TODO("Not yet implemented") }

            override fun onNothingSelected(parent: AdapterView<*>?) { TODO("Not yet implemented") }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { seleccionado[1] = position+2 }
        }

        binding.spnKit.onItemSelectedListener=object: AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { TODO("Not yet implemented") }

            override fun onNothingSelected(parent: AdapterView<*>?) { TODO("Not yet implemented") }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { seleccionado[2] = position+5 }
        }

        binding.btnSiguiente.setOnClickListener {
            var ventana = AlertDialog.Builder(this)
            ventana.setTitle("¿Pasar a las siguientes categorías?")
            ventana.setMessage("Tu personalización quedará guardada")
            ventana.setPositiveButton("Si"){ ventana, id ->
                try{
                    for ( i in 0 until 3){
                        guardadoUsuario?.add(operaciones.accesoriosInteriores.get(seleccionado[i]))
                    }
                    val pantallaAccesorios = Intent(this, AccesoriosActivity::class.java)
                    pantallaAccesorios.putExtra("guardados",guardadoUsuario)
                    startActivity(pantallaAccesorios)
                }catch (e: Exception){
                    println(e)
                }
            }
            ventana.setNegativeButton("No"){ventana, id->}
            ventana.show()
        }

    }
}