package mx.edu.utez.agenciadecoches

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.Toast
import mx.edu.utez.agenciadecoches.databinding.ActivitySummaryBinding
import mx.edu.utez.agenciadecoches.databinding.ActivitySummaryBinding.*

class SummaryActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivitySummaryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        var guardadoUsuario = intent.getSerializableExtra("guardados") as? ArrayList<AccesorioBean>
        var precio:Double=0.0

        guardadoUsuario?.forEach {
            precio+=it.precio
        }
        binding.txtStereo.text = guardadoUsuario?.get(0)?.nombre
        binding.txtMaterial.text = guardadoUsuario?.get(1)?.nombre
        binding.txtVidrios.text = guardadoUsuario?.get(2)?.nombre
        binding.txtRines.text = guardadoUsuario?.get(3)?.nombre
        binding.txtFaros.text = guardadoUsuario?.get(4)?.nombre
        binding.txtKit.text = guardadoUsuario?.get(5)?.nombre
        binding.txtQuemacocos.text = guardadoUsuario?.get(6)?.nombre
        binding.txtBolsas.text = guardadoUsuario?.get(7)?.nombre
        binding.txtAire.text = guardadoUsuario?.get(8)?.nombre
        binding.txtPrecioTotal.text = "Total de: \$ $precio MXN"

        binding.btnVolver.setOnClickListener {
            var ventana = AlertDialog.Builder(this)
            ventana.setTitle("¿Estás seguro de volver al inicio?")
            ventana.setMessage("Podrás crear otra personalización")
            ventana.setPositiveButton("Si"){ ventana, id ->
                val pantallaInicio = Intent(this, BienvenidaActivity::class.java)
                pantallaInicio.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(pantallaInicio)
            }
            ventana.setNegativeButton("No"){ventana, id-> }
            ventana.show()
        }

    }
}