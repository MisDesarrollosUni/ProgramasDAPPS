package mx.edu.utez.agenciadecoches

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import mx.edu.utez.agenciadecoches.databinding.ActivityAccesoriosBinding
import java.lang.Exception

class AccesoriosActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityAccesoriosBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val operaciones = Operaciones()
        operaciones.vaciarLista()
        operaciones.iniciarAccesorios()
        var seleccionado = IntArray(3)

        var guardadoUsuario = intent.getSerializableExtra("guardados") as? ArrayList<AccesorioBean>

        var adapQuemacocos: ArrayAdapter<String> = ArrayAdapter(this, R.layout.spn_elemento, operaciones.retornarQuemacocos())
        adapQuemacocos.setDropDownViewResource(R.layout.spn_elementos)
        binding.spnQuemacocos.adapter = adapQuemacocos

        var adapBolsas: ArrayAdapter<String> = ArrayAdapter(this, R.layout.spn_elemento, operaciones.retornarBolsas())
        adapBolsas.setDropDownViewResource(R.layout.spn_elementos)
        binding.spnBolsas.adapter = adapBolsas

        var adapAire: ArrayAdapter<String> = ArrayAdapter(this, R.layout.spn_elemento, operaciones.retornarAire())
        adapAire.setDropDownViewResource(R.layout.spn_elementos)
        binding.spnAire.adapter = adapAire

        binding.spnQuemacocos.onItemSelectedListener=object: AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { TODO("Not yet implemented") }

            override fun onNothingSelected(parent: AdapterView<*>?) { TODO("Not yet implemented") }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { seleccionado[0] = position }
        }

        binding.spnBolsas.onItemSelectedListener=object: AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { TODO("Not yet implemented") }

            override fun onNothingSelected(parent: AdapterView<*>?) { TODO("Not yet implemented") }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { seleccionado[1] = position+2 }
        }

        binding.spnAire.onItemSelectedListener=object: AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { TODO("Not yet implemented") }

            override fun onNothingSelected(parent: AdapterView<*>?) { TODO("Not yet implemented") }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { seleccionado[2] = position+4 }
        }

        binding.btnSiguiente.setOnClickListener {
            var ventana = AlertDialog.Builder(this)
            ventana.setTitle("¿Pasar a las siguientes categorías?")
            ventana.setMessage("Tu personalización quedará guardada")
            ventana.setPositiveButton("Si"){ ventana, id ->
                try{
                    for ( i in 0 until 3){
                        guardadoUsuario?.add(operaciones.accesoriosInteriores.get(seleccionado[i]))
                    }
                    val pantallaSummary = Intent(this, SummaryActivity::class.java)
                    pantallaSummary.putExtra("guardados",guardadoUsuario)
                    startActivity(pantallaSummary)
                }catch (e: Exception){
                    println(e)
                }
            }
            ventana.setNegativeButton("No"){ventana, id->}
            ventana.show()
        }
    }
}