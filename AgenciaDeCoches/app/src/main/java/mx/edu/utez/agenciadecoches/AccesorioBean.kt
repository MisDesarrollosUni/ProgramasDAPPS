package mx.edu.utez.agenciadecoches

import java.io.Serializable

class AccesorioBean:Serializable{
    var nombre:String = ""
    var precio:Double = 0.0

    constructor(nombre: String, precio: Double) {
        this.nombre = nombre
        this.precio = precio
    }
}



