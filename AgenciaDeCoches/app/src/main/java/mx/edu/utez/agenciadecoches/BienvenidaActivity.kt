package mx.edu.utez.agenciadecoches

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_bienvenida.*


class BienvenidaActivity : AppCompatActivity(), View.OnClickListener{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bienvenida)
        btnCrearAuto.setOnClickListener(this)
        btnSalir.setOnClickListener(this)
    }

    override fun onClick(boton: View?) {
        when(boton?.id){
            R.id.btnCrearAuto ->{
                val pantallaInteriores = Intent(this, InterioresActivity::class.java)
                startActivity(pantallaInteriores)
            }
            R.id.btnSalir -> {
                var ventana = AlertDialog.Builder(this)
                ventana.setTitle("¿Estás seguro?")
                ventana.setMessage("Saldrás de la aplicacion")
                ventana.setPositiveButton("Si"){ ventana, id ->
                    Toast.makeText(this@BienvenidaActivity, "Saliste", Toast.LENGTH_SHORT).show()
                    System.exit(0)
                }
                ventana.setNegativeButton("No"){ventana, id->}
                ventana.show()
            }
        }
    }


}
